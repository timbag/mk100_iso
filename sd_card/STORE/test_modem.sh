#!/bin/sh -x

init()
{
echo 0 > /sys/class/leds/gsm_ena_gsmpwr/brightness #power off
echo 0 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton not pressed
echo 0 > /sys/class/leds/gsm_usb_boot/brightness # normal boot
}

wait_dev()
{
	for cnt in `seq 1 $2`; do
		if [ -e "$1" ]; then
			break
		fi
		echo "wait $1"
		sleep 1
	done
}

enable()
{
echo 1 > /sys/class/leds/gsm_ena_gsmpwr/brightness #power on
sleep 1
echo 1 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton press
sleep 1
echo 0 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton not pressed
wait_dev /dev/ttyUSB0 1000
sleep 1
}

disable()
{
echo 1 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton press
sleep 1
echo 0 > /sys/class/leds/gsm_pwr_key/brightness # pwrbutton not pressed
sleep 3
echo 0 > /sys/class/leds/gsm_ena_gsmpwr/brightness #power off
sleep 1
}

disable
init
for i in `seq 1 1000`; do
	enable
	echo "enable done"
	disable
	echo "disable done"
done


