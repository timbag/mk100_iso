#!/bin/sh

cfg=/sys/kernel/config
mount none $cfg -t configfs
g1=${cfg}/usb_gadget/g1
mkdir ${g1}
mkdir ${g1}/configs/c.1
mkdir ${g1}/strings/0x409
mkdir ${g1}/configs/c.1/strings/0x409
echo 0x0525 > ${g1}/idVendor
echo 0xa4ac > ${g1}/idProduct
#echo 0x2001 > ${g1}/idVendor
#echo 0x4a00 > ${g1}/idProduct
echo serial > ${g1}/strings/0x409/serialnumber
echo manufacturer > ${g1}/strings/0x409/manufacturer
echo HID Gadget > ${g1}/strings/0x409/product
echo "Conf 1" > ${g1}/configs/c.1/strings/0x409/configuration
echo 120 > ${g1}/configs/c.1/MaxPower
cd /sys/kernel/config/usb_gadget/g1
#######do rndis
mkdir functions/rndis.0
ln -s functions/rndis.0 configs/c.1
echo ci_hdrc.0 > /sys/kernel/config/usb_gadget/g1/UDC #enable
ifconfig usb0 192.168.77.4 up



