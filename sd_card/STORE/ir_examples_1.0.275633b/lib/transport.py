import serial
import socket
import sys
import errno

class tcp_transport(socket.socket):
    def write(self, buf):
        self.sendall(buf)

    def read(self, lenght):
        buf = bytes(0)
        try:
            while len(buf) < lenght:
                buf += self.recv(lenght - len(buf))
        except socket.timeout:
            pass
        except OSError as e:
            if e.errno != errno.EINTR:
                raise
        return buf

class file_transport():
    def __init__(self, filename):
        self.file = open(filename, 'rb')

    def read(self, len):
        return self.file.read(len)

    def write(self, buf):
        pass

    def settimeout(self, timeout):
        pass

    def gettimeout(self):
        pass


class com_transport(serial.Serial):
    def settimeout(self, timeout):
        self.timeout = timeout

    def gettimeout(self):
        return self.timeout

    def enable_low_latency_mode(self):
        import array
        import fcntl
        import termios

        buf = array.array('i', [0] * 32)

        try:
            # get serial_struct
            fcntl.ioctl(self.fd, termios.TIOCGSERIAL, buf)

            # set ASYNC_LOW_LATENCY flag
            buf[4] |= 0x2000

            # set serial_struct
            fcntl.ioctl(self.fd, termios.TIOCSSERIAL, buf)
        except IOError as e:
            print('Warning: Failed to set ASYNC_LOW_LATENCY flag: {}'.format(e))

def default_uart_port():
    from sys import platform
    if platform == 'win32':
        return 'COM3'
    else:
        return '/dev/ttyUSB0'


def default_tcp_port():
    return [42616]

def choose_transport(args):
    ip_addr = args['--ip-addr']
    file_name = args['--file']
    port = args['--com-port'] or default_uart_port()
    baudrate = args['--baudrate'] or "115200"
    if file_name:
        print('Using response dump file', file_name)
        return file_transport(file_name)
    elif ip_addr:
        print('Using TCP', ip_addr)
        tcp = tcp_transport(socket.AF_INET, socket.SOCK_STREAM)
        addr, port = (ip_addr.split(':') + default_tcp_port())[:2]
        tcp.connect((addr, int(port)))
        return tcp
    else:
        print('Using UART', port)
        uart = com_transport(
            port=port,
            baudrate=int(baudrate),
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
        )
        uart.reset_input_buffer()

        plat = sys.platform.lower()
        if plat[:5] == 'linux':
            uart.enable_low_latency_mode()

        return uart
