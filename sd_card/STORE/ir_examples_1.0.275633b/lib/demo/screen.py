# encoding=utf8

import time

import intellireader.commands_pb2 as commands_pb2

import intellireader.gui.alignment_pb2 as alignment_pb2
import intellireader.gui.background_pb2 as background_pb2
import intellireader.gui.font_pb2 as font_pb2
import intellireader.gui.screen_pb2 as screen_pb2
import intellireader.gui.solidfill_pb2 as solidfill_pb2
import intellireader.gui.picture_id_pb2 as picture_id_pb2
import intellireader.gui.widget_pb2 as widget_pb2

import lib.proto

from . import leds, buzz, types

last = (0, '')

def tap_card(proto):
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout

    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add()

    text.text = 'ПРИЛОЖИТЕ\nКАРТУ'
    text.font = font_pb2.LARGE_FONT

    picture.picture_id = picture_id_pb2.EMV_CONTACTLESS_SYMBOL

    ps_line(bottom)

    show_screen(proto, 'tap_card', cmd)

def approve(proto, result):
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout

    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add()

    text.text = 'ОПЛАЧЕНО\n42 Руб'
    text.font = font_pb2.LARGE_FONT

    picture.picture_id = picture_id_pb2.APPROVED

    ps_line(bottom)

    show_screen(proto, 'approve', cmd)

def decline(proto, result):
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout
    
    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add()

    text.text = 'ОТКЛОНЕНО'
    text.font = font_pb2.LARGE_FONT

    picture.picture_id = picture_id_pb2.REJECTED

    ps_line(bottom)

    show_screen(proto, 'decline', cmd)

def use_contact_iface(proto, result):
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout
    
    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add()

    text.text = 'НЕ ОБСЛУ-\nЖИВАЕТСЯ'
    text.font = font_pb2.LARGE_FONT

    picture.picture_id = picture_id_pb2.REJECTED

    ps_line(bottom)

    show_screen(proto, 'use_contact_iface', cmd)

def transport_card(proto, result):
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout
    
    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add()

    text.text = types.token_type_text(proto, result)
    text.font = font_pb2.LARGE_FONT
    
    picture.picture_id = picture_id_pb2.APPROVED
    picture.vertical_alignment = alignment_pb2.OPTIMAL_VERTICALLY 

    ps_line(bottom)

    show_screen(proto, 'non_emv', cmd)

def expired(proto, result):
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout
    
    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add()

    text.text = 'ИСТЕК СРОК\nДЕЙСТВИЯ'
    text.font = font_pb2.LARGE_FONT

    picture.picture_id = picture_id_pb2.REJECTED

    ps_line(bottom)

    show_screen(proto, 'expired', cmd)

def cdcvm_required(proto, result):
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout
    
    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add()

    text.text = 'ОТКАЗ ОТ\nТЕЛЕФОНА'
    text.font = font_pb2.LARGE_FONT

    picture.picture_id = picture_id_pb2.REJECTED

    ps_line(bottom)

    show_screen(proto, 'cdcvm_required', cmd)

def non_emv(proto, result):
    leds.read_ok(proto)
    transport_card(proto, result)
    buzz.ok(proto)

def show_screen(proto, name, screen):
    global last
    last = (time.time(), name)

    request = commands_pb2.Gui()
    request.show_screen.CopyFrom(screen)

    return proto.exchange(proto.MOD_GUI, request, None)

def ps_line(widget):
    hl = widget.horizontal_layout

    visa = hl.widgets.add()
    visa.picture.picture_id = picture_id_pb2.VISA_LOGO
    visa.picture.vertical_alignment = alignment_pb2.BOTTOM
    visa.picture.horizontal_alignment = alignment_pb2.LEFT

    mc = hl.widgets.add()
    mc.picture.picture_id = picture_id_pb2.MASTERCARD_LOGO
    mc.picture.vertical_alignment = alignment_pb2.BOTTOM

    mir = hl.widgets.add()
    mir.picture.picture_id = picture_id_pb2.MIR_LOGO
    mir.picture.vertical_alignment = alignment_pb2.BOTTOM
    mir.picture.horizontal_alignment = alignment_pb2.RIGHT

