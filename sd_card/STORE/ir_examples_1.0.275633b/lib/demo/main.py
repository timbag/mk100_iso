import time

import intellireader.commands_pb2 as commands_pb2

import intellireader.contactless.transaction_pb2 as transaction_pb2
import intellireader.contactless.emv_removal_pb2 as emv_removal_pb2

import lib.proto

from . import screen, leds, buzz

def demo_controller(proto):
    """Demo Contoller Application that implements
    general message flow for transit solutions
    """

    leds.polling(proto)
    screen.tap_card(proto)

    while True:
        result = polling(proto)
        ACTIONS[result.status](proto, result)

def polling(proto):
    result = transaction_pb2.TransactionResult()

    emv_removal(proto)

    while not perform_txn(proto, result):
        if screen.last[1] != 'tap_card' and time.time() - screen.last[0] > 5:
            screen.tap_card(proto)

        leds.polling(proto)

    return result

def perform_txn(proto, result):
    request = commands_pb2.ContactlessLevel2()
    perform_txn = request.perform_transaction

    perform_txn.poll_for_token.timeout = 1000
    perform_txn.poll_for_token.poll_stm_sri512 = True

    perform_txn.amount_authorized = 100 # 1.00$
    perform_txn.transaction_date = b'\x13\x06\x13'
    perform_txn.transaction_time = b'\x12\x00\x00'
    perform_txn.transaction_type = b'\x00'
    perform_txn.terminal_country_code = b'\x06\x43'
    perform_txn.transaction_currency_code = b'\x06\x43'

    return proto.exchange(proto.MOD_CLESS_L2, request, result)

def emv_removal(proto):
    cmd = emv_removal_pb2.EmvRemoval()

    request = commands_pb2.ContactlessLevel1()
    request.emv_removal.SetInParent()

    return proto.exchange(proto.MOD_CLESS_L1, request, None)

ACTIONS = {
    transaction_pb2.ONLINE_AUTHORIZATION_REQUIRED: screen.approve,
    transaction_pb2.OFFLINE_APPROVED: screen.approve,
    transaction_pb2.OFFLINE_DECLINED: screen.decline,
    transaction_pb2.USE_CONTACT_INTERFACE: screen.use_contact_iface,
    transaction_pb2.UNABLE_PERFORM_TRANSACTION: screen.decline,
    transaction_pb2.NON_EMV_CARD: screen.non_emv,
    transaction_pb2.CARD_EXPIRED: screen.expired,
    transaction_pb2.AUTHORIZATION_ON_CARDHOLDER_DEVICE_REQUIRED: screen.cdcvm_required,
}
