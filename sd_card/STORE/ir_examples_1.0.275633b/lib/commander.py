from . import commands
from .commands import complex
from .commands import configuration
from .commands import contact
from .commands import contactless
from .commands import control
from .commands import gui
from .commands import mario
from .commands import mifare
from .commands import mifare_ext_samav2
from .commands import mifare_int_samav2
from .commands import misc
from .commands import ntag
from .commands import pinpad
from .commands import plus
from .commands import service
from .commands import stm_sri512
from .commands import touchscreen
from .commands import ultralight
from .commands import update

def approval_cless_l1_cmds():
    try:
        from .tests.cless_l1 import cmds as l1_cmds
        return l1_cmds
    except:
        return []

def approval_cless_l2_cmds():
    try:
        from .tests.cless_l2 import cmds as l2_cmds
        return l2_cmds
    except:
        return []

def gui_tests_cmds():
    try:
        from .tests.gui_tests import cmds as gui_cmds
        return gui_cmds
    except:
        return []

def mifare_tests_cmds():
    try:
        from .tests.mifare_tests import cmds as mifare_cmds
        return mifare_cmds
    except:
        return []

def get_commands():
    return complex.cmds + configuration.cmds + \
        contact.cmds + contactless.cmds + control.cmds + \
        gui.cmds + mario.cmds + mifare.cmds + \
        mifare_ext_samav2.cmds + mifare_int_samav2.cmds + \
        misc.cmds + ntag.cmds + pinpad.cmds + \
        plus.cmds + service.cmds + stm_sri512.cmds + \
        update.cmds + touchscreen.cmds + ultralight.cmds + \
        approval_cless_l1_cmds() + approval_cless_l2_cmds() + gui_tests_cmds() + mifare_tests_cmds()

def reload_commands():
    reload(complex)
    reload(configuration)
    reload(contact)
    reload(contactless)
    reload(control)
    reload(gui)
    reload(mario)
    reload(mifare)
    reload(mifare_ext_samav2)
    reload(mifare_int_samav2)
    reload(misc)
    reload(ntag)
    reload(pinpad)
    reload(plus)
    reload(service)
    reload(stm_sri512)
    reload(touchscreen)
    reload(ultralight)
    reload(update)

