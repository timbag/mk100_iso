from google.protobuf.text_format import *
from google.protobuf.message import *

import common.notification_pb2 as notification_pb2
import common.failure_pb2 as failure_pb2

from struct import *
from .formatter import hex_format
import crcmod
from termcolor import cprint
import socket

crc_func = crcmod.predefined.mkCrcFun('xmodem')
error_color = 'red'
tmp_dump_lvl = None

class Protocol:
    """Implements IntelliReader protocol handling"""

    MOD_ALL         = 0x00
    MOD_MISC        = 0x01
    MOD_CONTACT_L1  = 0x02
    MOD_CLESS_L1    = 0x03
    MOD_CLESS_L2    = 0x04
    MOD_MIFARE      = 0x05
    MOD_PINPAD      = 0x06
    MOD_SRV         = 0x07
    MOD_STMCARD     = 0x08
    MOD_NTAGCARD    = 0x09
    MOD_GUI         = 0x0A
    MOD_TOUCHSCREEN = 0x0B
    MOD_COMPLEX     = 0x0C

    TYPE_CMD = 0x01
    TYPE_RSP = 0x02
    TYPE_FAIL = 0x03
    TYPE_PEND = 0x04
    TYPE_NOTE = 0x05
    TYPE_CTRL = 0x06

    HEADER = b'IR'

    def __init__(self, transport, dump_lvl, counter):
        global tmp_dump_lvl
        tmp_dump_lvl = None

        self.timeout = 10
        self.counter = int(counter)
        self.transport = transport
        self.transport.settimeout(self.timeout)
        self.dump_lvl = dump_lvl
        self.is_hard_cancel_required = False

    def flush_rx(self):
        try:
            self.transport.settimeout(0)
            while True:
                data = self.transport.read(1)
                if len(data) == 0:
                    break
        except:
            pass
        finally:
            self.transport.settimeout(self.timeout)

    def exchange(self, mod, req, rsp):
        self.is_hard_cancel_required = False
        self.flush_rx()
        self.send(mod, req, self.TYPE_CMD)
        return self.recv(mod, rsp)

    def send(self, mod, request, msg_type):
        hdr_buf = self.HEADER

        if msg_type != self.TYPE_CTRL:
            self.counter += 1
            self.counter %= 256
            req_buf = pack("B", self.counter)
        else:
            req_buf = pack("B", 0)

        req_buf += pack("B", mod)
        req_buf += pack("B", msg_type)

        if request:
            req_buf += request.SerializeToString()

        req_len = len(req_buf)
        len_buf = pack(">H", req_len)

        crc = crc_func(hdr_buf + len_buf + req_buf)
        crc_buf = pack(">H", crc)

        self.dump(">>>", self.counter, request, len_buf, req_buf, crc_buf)

        msg = hdr_buf + len_buf + req_buf + crc_buf
        self.transport.write(msg)

    def recv(self, mod, response):
        local_counter = self.counter
        pending = False
        spinner = Spinner()

        while True:
            hdr_buf = self.transport.read(2)

            if len(hdr_buf) == 0:
                if pending is True:
                    next(spinner)
                    continue

                return self.error('ERR: NO RESPONSE RECEIVED DURING {} SECONDS'.format(self.transport.gettimeout()))

            if hdr_buf != self.HEADER:
                return self.error('ERR: UNKNOWN MESSAGE HEADER: {}'.format(hdr_buf.hex()))

            len_buf = self.transport.read(2)
            rsp_len = unpack(">H", len_buf)[0]

            rsp_buf = self.transport.read(rsp_len)
            rsp_cnt = unpack("B", rsp_buf[0:1])[0]
            rsp_mod = unpack("B", rsp_buf[1:2])[0]
            msg_type = unpack("B", rsp_buf[2:3])[0]

            crc_buf = self.transport.read(2)
            if not len(crc_buf):
                return self.error('ERR: NO CHECKSUM FOUND IN THE MESSAGE')

            got_crc = unpack(">H", crc_buf)[0]
            exp_crc = crc_func(hdr_buf + len_buf + rsp_buf)
            if exp_crc != got_crc:
                return self.error('ERR: WRONG CHECKSUM: expected {:#x} got {:#x}'.format(exp_crc, got_crc))

            if msg_type == self.TYPE_NOTE:
                payload = notification_pb2.Notification()
                payload.ParseFromString(rsp_buf[3:])

                if payload.HasField("log_message"):
                    note = payload.log_message

                    importance_decriptor = note.DESCRIPTOR.enum_types_by_name['Importance']
                    level = importance_decriptor.values_by_number[note.level].name

                    print("NOTE: " + level + "({})".format(note.level) + " " + str(note.msg))

                if payload.HasField("user_message"):
                    user = payload.user_message

                    print("USER: " + MessageToString(user, True, True))

                continue

            if msg_type == self.TYPE_PEND:
                self.dump("ACK", rsp_cnt, None, len_buf, rsp_buf, crc_buf)
                pending = True
                continue

            if msg_type == self.TYPE_FAIL:
                fail_rsp = failure_pb2.FailureResponse()
                fail_rsp.ParseFromString(rsp_buf[3:])

                self.dump("FAL", rsp_cnt, fail_rsp, len_buf, rsp_buf, crc_buf)
                return False

            if rsp_cnt != local_counter:
                return self.error('ERR: MISMATCHED COUNTER: expected {:03} got {:03}'.format(local_counter, rsp_cnt))

            if rsp_mod != mod:
                return self.error('ERR: MISMATCHED MODULE: expected {:03} got {:03}'.format(mod, rsp_mod))

            if msg_type == self.TYPE_RSP:
                if response:
                    response.ParseFromString(rsp_buf[3:])
                self.dump("<<<", rsp_cnt, response, len_buf, rsp_buf, crc_buf)
                return True

            return self.error('ERR: UNKNOWN MESSAGE TYPE: {:02X}'.format(msg_type))

    def dump(self, name, cnt, msg, len_buf, body_buf, crc_buf):
        if not self.dump_lvl and name != 'FAL':
            return

        dump = dump_short(name, cnt, msg)

        if self.dump_lvl == 'hex':
            dump += dump_hex(len_buf, body_buf, crc_buf)
        if self.dump_lvl == 'full':
            dump += dump_full(len_buf, body_buf, crc_buf)

        if name != 'FAL':
            print(dump)
        else:
            self.error(dump)

    def suspend_dump(self):
        global tmp_dump_lvl
        if not tmp_dump_lvl:
            self.dump_lvl, tmp_dump_lvl = tmp_dump_lvl, self.dump_lvl

    def restore_dump(self):
        global tmp_dump_lvl
        if tmp_dump_lvl:
            self.dump_lvl, tmp_dump_lvl = tmp_dump_lvl, self.dump_lvl

    def error(self, text):
        cprint(text, error_color)
        return False

    def hard_cancel(self):
        if self.is_hard_cancel_required:
            return True

        cprint('\nCancelling command...')
        self.send(self.MOD_ALL, None, self.TYPE_CTRL)
        self.is_hard_cancel_required = True
        return False

class Spinner:
    CHARS = '/-\\|'

    def __init__(self):
        self.pos = 0

    def __next__(self):
        import sys
        sys.stdout.write('{}\r'.format(self.CHARS[self.pos]))
        sys.stdout.flush()

        self.pos += 1
        self.pos %= len(self.CHARS)


def dump_short(name, cnt, msg):
    if msg and msg.ByteSize() > 0:
        text = MessageToString(msg, True, True, message_formatter=hex_format)
        if not text:
            raise ValueError("IRC is unable to parse payload. Maybe IRC is too old?")
    else:
        text = 'Empty'

    return name + " " + '{:03}'.format(cnt) + ": '" + text + "'"


def dump_hex(len_buf, body_buf, crc_buf=''):
    return '\nLEN:{} BODY:{} CRC:{}'.format(len_buf.hex(), body_buf.hex(), crc_buf.hex())


def dump_full(len_buf, body_buf, crc_buf):
    chksum = crc_buf.hex().upper()
    chkoff = 2 + len(len_buf) + len(body_buf)

    header = Protocol.HEADER.hex().upper()
    length = len_buf.hex().upper()

    counter = unpack("B", body_buf[0:1])[0]
    mod = unpack("B", body_buf[1:2])[0]
    typ = unpack("B", body_buf[2:3])[0]

    payload = body_buf[3:]

    dump = '\n'.join([
        "",
        "      HEADER LENGTH MSGID MODULE TYPE",
        "0000  {}   {}   {:0>2X}    {:0>2X}     {:0>2X}".format(header, length, counter, mod, typ),

        "",
        "      PAYLOAD",
        hexdump(payload),

        "      CHECKSUM",
        "{:04X}  {}".format(chkoff, chksum),
    ])

    return dump


def hexdump(src, offlen=7, length=16):
    FILTER = ''.join([(len(repr(chr(x))) == 3) and chr(x) or '.' for x in range(256)])
    lines = []
    for c in range(0, len(src), length):
        chars = src[c:c+length]
        hex = ' '.join(["%02X" % x for x in chars])
        printable = ''.join(["%s" % ((x <= 127 and FILTER[x]) or '.') for x in chars])
        lines.append("%04X  %-*s  %s\n" % (c + offlen, length*3, hex, printable))
    return ''.join(lines)
