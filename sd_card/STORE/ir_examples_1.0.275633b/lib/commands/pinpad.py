from google.protobuf.text_format import *
from google.protobuf.message import *

import intellireader.commands_pb2 as commands_pb2

sc_slot = 'slot: MAIN_SLOT'
mod_0137 = "aa8b4802f37cde759f97f8cba4b51034bd5b622494746b64a90f39767b319fb1" \
         + "98b8d7c7635b510ed2b95aa89d705f57cfcb11be7ecc621f176f642a7d3938b8" \
         + "a1dafa87d113abe0d6aeeb84aea2686f55bc31facc28e4f2f1a0f71684fc47cf" \
         + "1240a9aa04ae20a5daa6c224ab1cc09f"

def online_pin(proto, payload):
    """ Ask cardholder to enter PIN & return PIN-block """

    import intellireader.pinpad.online_pin_pb2 as online_pin_pb2
    import intellireader.pinpad.key_bundle_pb2 as key_bundle_pb2

    request = commands_pb2.PinPad()
    cmd = request.online_pin

    cmd.pan = b"432198765432109870"
    cmd.key_format = key_bundle_pb2.TR31

    # AES, ISO 9564 Format 4
    cmd.session_key = \
        b"D0112P0AE00E0000B82679114F470F540165EDFBF7E250FCEA43F810D215F8D207E2E417C07156A27E8E31DA05F7425509593D03A457DC34"
    cmd.pin_block_format = online_pin_pb2.ISO_9564_FORMAT_4

    if payload:
        Merge(payload, cmd)

    return proto.exchange(proto.MOD_PINPAD, request, online_pin_pb2.EntryResult())

def verify_offline_pin(proto, payload):
    """ Ask cardholder to enter PIN and sends it to card using integrated SmartCard reader """

    import intellireader.pinpad.offline_pin_pb2 as offline_pin_pb2

    request = commands_pb2.PinPad()
    cmd = request.offline_pin.enciphered_pin

    # cmd = request.offline_pin.plaintext_pin
    # cmd.SetInParent()

    cmd.public_key.modulus = bytes.fromhex(mod_0137)
    cmd.public_key.exponent = b"\x03"

    if payload:
        Merge(payload, cmd)

    return proto.exchange(proto.MOD_PINPAD, request, offline_pin_pb2.VerifyResult())

def offline_pin(proto, payload):
    """ Offline PIN processing using integrated SmartCard reader """

    from . import contact as smartcard

    smartcard.icc_power_off(proto, sc_slot)

    if not smartcard.icc_power_on(proto, sc_slot):
        return False

    # Select MasterCard application
    select = sc_slot + " command_apdu: \"\\x00\\xA4\\x04\\x00\\x07\\xA0\\x00\\x00\\x00\\x04\\x10\\x10\\x00\""
    smartcard.icc_transmit(proto, select)

    # Get Processing Options
    gpo = sc_slot + " command_apdu: \"\\x80\\xA8\\x00\\x00\\x02\\x83\\x00\\x00\""
    smartcard.icc_transmit(proto, gpo)

    # Enter Offline PIN and verify it by the SmartCard
    verify_offline_pin(proto, payload)

    smartcard.icc_power_off(proto, sc_slot)

cmds = [online_pin, offline_pin]
