# encoding=utf8

from google.protobuf.text_format import *
from google.protobuf.message import *

import intellireader.commands_pb2 as commands_pb2

import intellireader.gui.alignment_pb2 as alignment_pb2
import intellireader.gui.background_pb2 as background_pb2
import intellireader.gui.font_pb2 as font_pb2
import intellireader.gui.screen_pb2 as screen_pb2
import intellireader.gui.solidfill_pb2 as solidfill_pb2
import intellireader.gui.picture_id_pb2 as picture_id_pb2
import intellireader.gui.widget_pb2 as widget_pb2

def ps_line(widget):
    hl = widget.horizontal_layout

    visa = hl.widgets.add()
    visa.picture.picture_id = picture_id_pb2.VISA_LOGO
    visa.picture.vertical_alignment = alignment_pb2.BOTTOM
    visa.picture.horizontal_alignment = alignment_pb2.LEFT

    mc = hl.widgets.add()
    mc.picture.picture_id = picture_id_pb2.MASTERCARD_LOGO
    mc.picture.vertical_alignment = alignment_pb2.BOTTOM

    mir = hl.widgets.add()
    mir.picture.picture_id = picture_id_pb2.MIR_LOGO
    mir.picture.vertical_alignment = alignment_pb2.BOTTOM
    mir.picture.horizontal_alignment = alignment_pb2.RIGHT

def screen1():
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout

    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add()

    text.text = 'ПРИЛОЖИТЕ\nКАРТУ'
    text.font = font_pb2.LARGE_FONT

    picture.picture_id = picture_id_pb2.EMV_CONTACTLESS_SYMBOL

    ps_line(bottom)

    return cmd

def screen2():
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout

    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add()

    text.text = 'ОПЛАЧЕНО\n42 Руб'
    text.font = font_pb2.LARGE_FONT

    picture.picture_id = picture_id_pb2.APPROVED

    ps_line(bottom)

    return cmd

def screen3():
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout
    
    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add()

    text.text = 'КАРТА В\nСТОП-ЛИСТЕ'
    text.font = font_pb2.LARGE_FONT

    picture.picture_id = picture_id_pb2.REJECTED

    ps_line(bottom)

    return cmd

def screen4():
    from irc import version

    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout

    text = vl.widgets.add().text
    text.text = "IntelliReader"
    text.font = font_pb2.LARGE_FONT
    text.vertical_alignment = alignment_pb2.OPTIMAL_VERTICALLY 

    qr = vl.widgets.add().qr_code
    qr.text = "Visit https://valitek.ru/lite for more information".format(version()).encode()

    text = vl.widgets.add().text
    text.text = "Visit https://valitek.ru\nfor more information"
    text.vertical_alignment = alignment_pb2.OPTIMAL_VERTICALLY

    return cmd

def screen5():
    cmd = commands_pb2.Gui().show_screen
    vl = cmd.root.vertical_layout

    text = vl.widgets.add().text
    picture = vl.widgets.add().picture
    bottom = vl.widgets.add().vertical_layout

    text.text = 'ПРИЛОЖИТЕ\nКАРТУ'
    text.font = font_pb2.LARGE_FONT

    picture.picture_id = picture_id_pb2.EMV_CONTACTLESS_SYMBOL

    empty = bottom.widgets.add()
    button = bottom.widgets.add()

    button.text.text = 'OПЛАТA БАГАЖА'
    button.text.background.solid_fill_rgb = 0x99FFFF
    button.text.foreground.solid_fill_rgb = 0x000000
    button.text.button_id = 0

    return cmd

def screen6():
    cmd = commands_pb2.Gui().show_screen
    root = cmd.root

    # the picture from config.zip
    root.customer_picture.name = "luggage.bmp"

    return cmd

def show_screen(proto, payload):
    """ Show user-define screen on reader's display"""

    if not payload:
        screen = globals()["screen1"]
        payload = MessageToString(screen())
    else:
        screen = globals().get(payload)

        if screen:
            payload = MessageToString(screen())

    request = commands_pb2.Gui()
    cmd = request.show_screen
    Merge(payload, cmd)

    return proto.exchange(proto.MOD_GUI, request, None)

cmds = [show_screen]
