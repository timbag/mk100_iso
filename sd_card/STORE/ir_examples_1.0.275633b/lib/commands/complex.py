import time

from google.protobuf.text_format import *
from google.protobuf.message import *

import intellireader.commands_pb2 as commands_pb2

import lib.proto

def poll_for_event(proto, payload):
    """ Poll for an event from multiple reader interfaces """

    import intellireader.complex.poll_for_event_pb2 as poll_for_event_pb2
    import intellireader.touchscreen.poll_touchscreen_pb2 as poll_touchscreen_pb2

    request = commands_pb2.Complex()
    cmd = request.poll_for_event

    cmd.perform_txn.poll_for_token.timeout = 10 * 1000
    cmd.perform_txn.poll_for_token.poll_stm_sri512 = True
    cmd.perform_txn.poll_for_token.prefer_mifare = False

    cmd.perform_txn.amount_authorized = 100 # 1.00$
    cmd.perform_txn.transaction_date = b'\x13\x06\x13'
    cmd.perform_txn.transaction_time = b'\x12\x00\x00'
    cmd.perform_txn.transaction_type = b'\x00'
    cmd.perform_txn.terminal_country_code = b'\x06\x43'
    cmd.perform_txn.transaction_currency_code = b'\x06\x43'

    cmd.poll_touchscreen.event_type = poll_touchscreen_pb2.WIDGET_ID

    if payload:
        Merge(payload, cmd)

    return proto.exchange(proto.MOD_COMPLEX, request, poll_for_event_pb2.Event())

cmds = [poll_for_event]
