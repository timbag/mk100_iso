from Crypto.Cipher import AES
from Crypto.Hash import CMAC
from Crypto.Random import get_random_bytes

from google.protobuf.text_format import *
from google.protobuf.message import *

import intellireader.contact.card_slot_pb2 as card_slot_pb2

import lib.proto

SLOT = card_slot_pb2.SAM2_SLOT
MASTER_KEY = bytearray.fromhex("E571800C723099603504239A26C4B641")

def sam_transmit(proto, cmd):
    import intellireader.commands_pb2 as commands_pb2
    import intellireader.contact.iso7816_4_pb2 as iso7816_4_pb2

    request = commands_pb2.ContactLevel1()

    request.transmit_apdu.slot = SLOT
    request.transmit_apdu.command_apdu = bytes(cmd)

    rsp = iso7816_4_pb2.ResponseApdu()

    if not proto.exchange(proto.MOD_CONTACT_L1, request, rsp):
        raise ValueError("Transmit APDU failed")

    return rsp.body, rsp.trailer

def cmac(key, data):
    cobj = CMAC.new(key, ciphermod=AES)
    cobj.update(bytearray(data))
    mac = cobj.digest()
    mac = list(bytearray(mac))
    return mac[1::2]

def aes_encrypt(key, data):
    cobj = AES.new(key, AES.MODE_CBC, iv = bytearray(16))
    msg = cobj.encrypt(bytearray(data))
    return bytearray(msg)

def aes_decrypt(key, data):
    cobj = AES.new(key, AES.MODE_CBC, iv = bytearray(16))
    msg = cobj.decrypt(bytearray(data))
    return bytearray(msg)

def derive_key(key, rnd1, rnd2):
    sv1 = rnd1[7:12] + rnd2[7:12] + \
        list(a ^ b for a, b in zip(rnd1[0:5], rnd2[0:5])) + \
        [0x91]

    return aes_encrypt(key, sv1)

def av2_unlock(proto, payload):
    """ Unlock Mifare SAM AV2 without providing Master Key to IntelliReader """

    first_part = [0x80, 0xA4, 0x00, 0x00, 0x03, 0x01, 0x00, 0x00, 0x00]
    rsp1, sw12 = sam_transmit(proto, first_part)
    if sw12 != 0x90AF:
        return False

    rnd2 = list(bytearray(rsp1))
    mac = cmac(MASTER_KEY, rnd2 + [0x00] + [0x00] * 3)
    rnd1 = list(bytearray(get_random_bytes(12)))

    second_part = [0x80, 0xA4, 0x00, 0x00, 0x14] + mac + rnd1 + [0x00]
    rsp2, sw12 = sam_transmit(proto, second_part)
    if sw12 != 0x90AF:
        return False

    rnda = list(bytearray(get_random_bytes(16)))
    encrypted_rndb = rsp2[8:]

    session_key = derive_key(MASTER_KEY, rnd1, rnd2)
    rndb = aes_decrypt(session_key, encrypted_rndb)
    rndb = list(rndb)
    rndab = rnda + rndb[2:] + rndb[:2]
    encrypted_rndab = aes_encrypt(session_key, rndab)
    encrypted_rndab = list(encrypted_rndab)

    third_part = [0x80, 0xA4, 0x00, 0x00, 0x20] + encrypted_rndab + [0x00]
    encrypted_rnda_, sw12 = sam_transmit(proto, third_part)
    if sw12 != 0x9000:
        return False

    return True

cmds = [av2_unlock]
