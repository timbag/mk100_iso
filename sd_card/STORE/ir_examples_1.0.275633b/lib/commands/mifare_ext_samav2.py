from struct import *
import time

from Crypto.Hash import CMAC
from Crypto.Cipher import AES

from google.protobuf.text_format import *
from google.protobuf.message import *

import intellireader.commands_pb2 as commands_pb2
import intellireader.contactless.transceive_pb2 as transceive_pb2

import lib.proto
from . import misc

MASTER_KEY = bytearray.fromhex("E571800C723099603504239A26C4B641")
KEY_NUMBER = 0x0E
BLOCK_NUMBER = 0x04

def mfr_transmit(proto, tx_data, options):
    request = commands_pb2.ContactlessLevel1()
    cmd = request.tsv_bit_array

    cmd.bit_array.data = tx_data
    cmd.bit_array.count = len(tx_data) * 8
    cmd.response_timeout_us = 1000

    Merge(options, cmd)

    response = transceive_pb2.BitArray()
    if not proto.exchange(proto.MOD_CLESS_L1, request, response):
        return None

    return response.data

def sam_transmit(connection, cmd):
    print(">>> SAM: " + ''.join('{:02X}'.format(a) for a in cmd))
    rsp, sw1, sw2 = connection.transmit(cmd)
    print("<<< SAM: " + ''.join('{:02X}'.format(a) for a in rsp) + " {:02X}{:02X}".format(sw1, sw2))
    return rsp, unpack(">H", bytes([sw1, sw2]))[0]

def cmac(key, data):
    cobj = CMAC.new(key, ciphermod=AES)
    cobj.update(bytearray(data))
    mac = cobj.digest()
    mac = list(bytearray(mac))
    return mac[1::2]

def aes_encrypt(key, data):
    cobj = AES.new(key, AES.MODE_CBC, iv = bytearray(16))
    msg = cobj.encrypt(bytearray(data))
    return bytearray(msg)

def aes_decrypt(key, data):
    cobj = AES.new(key, AES.MODE_CBC, iv = bytearray(16))
    msg = cobj.decrypt(bytearray(data))
    return bytearray(msg)

def derive_key(key, rnd1, rnd2):
    sv1 = rnd1[7:12] + rnd2[7:12] + \
        list(a ^ b for a, b in zip(rnd1[0:5], rnd2[0:5])) + \
        [0x91]

    return aes_encrypt(key, sv1)

def host_auth(connection, key):
    from Crypto.Random import get_random_bytes

    print("Unlocking SAM AV2 by Host Authentication...")

    first_part = [0x80, 0xA4, 0x00, 0x00, 0x03, 0x01, 0x00, 0x00, 0x00]
    rnd2, sw12 = sam_transmit(connection, first_part)
    if sw12 != 0x90AF:
        return False

    mac = cmac(key, rnd2 + [0x00] + [0x00] * 3)
    rnd1 = list(get_random_bytes(12))

    second_part = [0x80, 0xA4, 0x00, 0x00, 0x14] + mac + rnd1 + [0x00]
    rsp2, sw12 = sam_transmit(connection, second_part)
    if sw12 != 0x90AF:
        return False

    rnda = list(get_random_bytes(16))
    encrypted_rndb = rsp2[8:]

    session_key = derive_key(key, rnd1, rnd2)
    rndb = aes_decrypt(session_key, encrypted_rndb)
    rndb = list(rndb)
    rndab = rnda + rndb[2:] + rndb[:2]
    encrypted_rndab = aes_encrypt(session_key, rndab)
    encrypted_rndab = list(encrypted_rndab)

    third_part = [0x80, 0xA4, 0x00, 0x00, 0x20] + encrypted_rndab + [0x00]
    encrypted_rnda_, sw12 = sam_transmit(connection, third_part)
    if sw12 != 0x9000:
        return False

    print("Done\n")
    return True

def poll_for_token(proto):
    import intellireader.contactless.poll_for_token_pb2 as poll_for_token_pb2
    import intellireader.contactless.token_pb2 as token_pb2

    print("Poll for token...")

    request = commands_pb2.ContactlessLevel1()
    cmd = request.poll_for_token

    cmd.timeout = 15000

    token = token_pb2.Token()
    if not proto.exchange(proto.MOD_CLESS_L1, request, token):
        return None

    print("Done\n")
    return list(bytearray(token.id))[:4]

def mfr_authentication(connection, proto, uid):
    print("Authenticating to Mifare sector...")
    t0 = time.time()

    mifare_auth = bytes([0x60, BLOCK_NUMBER])
    number_rb = mfr_transmit(proto, mifare_auth, 'tx_crc: true rx_crc: false parity: true')
    if not number_rb:
        return False
    number_rb = list(number_rb)

    # Key Entry number, version 0x00, Key Type 0x0A 
    samav2_key_entry = [KEY_NUMBER, 0x00, 0x0A] 
    # Block Number, "number RB"
    mifare_info = [BLOCK_NUMBER] + number_rb

    first_part = \
        [0x80, 0x1C, 0x00, 0x00] + [0x0D] + \
        uid + samav2_key_entry + mifare_info + [0x00] + \
        [0x00]

    token_ab, sw12 = sam_transmit(connection, first_part)
    if sw12 != 0x90AF:
        return False

    token_ab = bytes(token_ab)
    token_ba = mfr_transmit(proto, token_ab, '')
    if not token_ba:
        return False
    token_ba = list(token_ba)

    second_part = [0x80, 0x1C, 0x00, 0x00] + [0x05] + token_ba
    _, sw12 = sam_transmit(connection, second_part)
    if sw12 != 0x9000:
        return False

    elapsed = time.time() - t0
    print("Done. Authentication took {elapsed:.2f} ms\n".format(elapsed=elapsed * 1000))

    return True

def mfr_ext_samav2(proto, payload):
    """ Mifare Classic Authentication using SAM AV2 through PCSC-reader """

    try:
        from smartcard.System import readers
    except ImportError:
        print("""
          To use this command you need:
            - Install `pyscard` package by `pip install pyscard`
            - On Linux: install `libpcsclite-dev` and `pcscd` packages by default package manager
        """)
        return False

    print("\nConnecting to SmartCard reader...")
    reader = readers()[0]
    print(reader)

    connection = reader.createConnection()
    connection.connect()
    print("Done\n")

    if not host_auth(connection, MASTER_KEY):
        return False

    uid = poll_for_token(proto)
    if not uid:
        return False

    if not mfr_authentication(connection, proto, uid):
        return False

    return True

cmds = [mfr_ext_samav2]
