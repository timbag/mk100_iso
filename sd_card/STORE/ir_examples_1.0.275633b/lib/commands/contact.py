from google.protobuf.text_format import *
from google.protobuf.message import *

import intellireader.commands_pb2 as commands_pb2
import intellireader.contact.card_slot_pb2 as card_slot_pb2

import lib.proto

def icc_power_on(proto, payload):
    """ Power ON contact SmartCard """

    import intellireader.contact.power_on_pb2 as power_on_pb2

    request = commands_pb2.ContactLevel1()
    cmd = request.power_on_card
    cmd.slot = card_slot_pb2.SAM3_SLOT

    if payload:
        Merge(payload, cmd)

    return proto.exchange(proto.MOD_CONTACT_L1, request, power_on_pb2.ContactCard())

def icc_power_off(proto, payload):
    """ Power OFF contact SmartCard """

    import intellireader.contact.power_off_pb2 as power_off_pb2

    request = commands_pb2.ContactLevel1()
    cmd = request.power_off_card
    cmd.slot = card_slot_pb2.SAM3_SLOT

    if payload:
        Merge(payload, cmd)

    return proto.exchange(proto.MOD_CONTACT_L1, request, None)

def icc_transmit(proto, payload):
    """ Transmit APDU (SAM AV2 Get Version by default) to SmartCard """

    import intellireader.contact.iso7816_4_pb2 as iso7816_4_pb2

    request = commands_pb2.ContactLevel1()
    cmd = request.transmit_apdu
    cmd.slot = card_slot_pb2.SAM3_SLOT
    cmd.command_apdu = b"\x80\x60\x00\x00\x00"

    if payload:
        Merge(payload, cmd)

    return proto.exchange(proto.MOD_CONTACT_L1, request, iso7816_4_pb2.ResponseApdu())

cmds = [icc_power_on, icc_power_off, icc_transmit]
