# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: misc/stats.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from misc import lwip_pb2 as misc_dot_lwip__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='misc/stats.proto',
  package='misc.stats',
  syntax='proto2',
  serialized_pb=_b('\n\x10misc/stats.proto\x12\nmisc.stats\x1a\x0fmisc/lwip.proto\"\x14\n\x12GetDeviceStatistic\"5\n\x0f\x44\x65viceStatistic\x12\"\n\x04lwip\x18\x01 \x01(\x0b\x32\x14.misc.lwip.Statistic')
  ,
  dependencies=[misc_dot_lwip__pb2.DESCRIPTOR,])




_GETDEVICESTATISTIC = _descriptor.Descriptor(
  name='GetDeviceStatistic',
  full_name='misc.stats.GetDeviceStatistic',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=49,
  serialized_end=69,
)


_DEVICESTATISTIC = _descriptor.Descriptor(
  name='DeviceStatistic',
  full_name='misc.stats.DeviceStatistic',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='lwip', full_name='misc.stats.DeviceStatistic.lwip', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=71,
  serialized_end=124,
)

_DEVICESTATISTIC.fields_by_name['lwip'].message_type = misc_dot_lwip__pb2._STATISTIC
DESCRIPTOR.message_types_by_name['GetDeviceStatistic'] = _GETDEVICESTATISTIC
DESCRIPTOR.message_types_by_name['DeviceStatistic'] = _DEVICESTATISTIC
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

GetDeviceStatistic = _reflection.GeneratedProtocolMessageType('GetDeviceStatistic', (_message.Message,), dict(
  DESCRIPTOR = _GETDEVICESTATISTIC,
  __module__ = 'misc.stats_pb2'
  # @@protoc_insertion_point(class_scope:misc.stats.GetDeviceStatistic)
  ))
_sym_db.RegisterMessage(GetDeviceStatistic)

DeviceStatistic = _reflection.GeneratedProtocolMessageType('DeviceStatistic', (_message.Message,), dict(
  DESCRIPTOR = _DEVICESTATISTIC,
  __module__ = 'misc.stats_pb2'
  # @@protoc_insertion_point(class_scope:misc.stats.DeviceStatistic)
  ))
_sym_db.RegisterMessage(DeviceStatistic)


# @@protoc_insertion_point(module_scope)
